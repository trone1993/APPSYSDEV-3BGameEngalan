//The Phaser 2  just dont work in chorme//



var canvas = document.querySelector("canvas")
var ctx = canvas.getContext("2d")

//size of the ball
var ballRadius = 10

//canvas
var x = canvas.width / 2
var y = canvas.height - 30

//draws 
var dx = 2
var dy = -2

//paddle
var paddleHeight = 13
var paddleWidth = 100
var paddleX = (canvas.width - paddleWidth) / 3

//controls
var rightPressed = false
var leftPressed = false

//bricks
var brickRowCount = 5
var brickColumnCount = 7
var brickWidth = 73
var brickHeight = 20
var brickPadding = 1
var brickOffsetTop = 30
var brickOffsetLeft = 30

//scorebroard & lives 
var score = 0
var lives = 4


//bricks orentation
var bricks = []
for(c = 0; c < brickColumnCount; c++) {
    bricks[c] = []
    for(r = 0;r<brickRowCount;r++) {
        bricks[c][r] = { x:0,y:0,status:1 }
    }
}

document.addEventListener("keydown", keyDownHandler)
document.addEventListener("keyup", keyUpHandler)

function keyDownHandler(event) {
    if (event.keyCode == 39) {
        rightPressed = true
    }
    else if(event.keyCode == 37) {
        leftPressed = true
    }
}

function keyUpHandler(event) {
    if (event.keyCode == 39) {
        rightPressed = false
    }
    else if (event.keyCode == 37) {
        leftPressed = false
    }
}

//collision for the ball and bricks and walls
function collisionDetection() {
    for(c = 0;c<brickColumnCount; c++) {
        for(r =0; r < brickRowCount; r++) {
            var b = bricks[c][r]
            if (b.status == 1) {
                if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                    dy = -dy
                    b.status = 0
                    score++
                    if (score == brickRowCount * brickColumnCount) {
                      alert("Great Job, You Win!!")
                      document.location.reload()  
                    }
                } 
            }    
        }
    }
}

//draw a ball
function drawBall() {
    ctx.beginPath()
    ctx.arc(x, y,ballRadius,0,Math.PI * 2)
    ctx.fillStyle = "#ffffff"
    ctx.fill()
    ctx.closePath()   
}

//draw the paddle
function drawPaddle() {
    ctx.beginPath()
    ctx.rect(paddleX,canvas.height - paddleHeight, 
    paddleWidth, paddleHeight)
    ctx.fillStyle = "#ffffff"
    ctx.fill()
    ctx.closePath()
}

//draw the bricks
function drawBricks() {
    for(c = 0; c < brickColumnCount; c++) {
        for(r = 0; r < brickRowCount; r++) {
            if (bricks[c][r].status == 1) {
                var brickX=(c*(brickWidth + brickPadding))+brickOffsetLeft
                var brickY=(r*(brickHeight + brickPadding))+brickOffsetTop
     
                bricks[c][r].x = brickX
                bricks[c][r].y = brickY
     
                ctx.beginPath()
                ctx.rect(brickX,brickY,brickWidth,brickHeight)
                ctx.fillStyle = "#ffffff"
                ctx.fill()
                ctx.closePath()
            }
        }
    }
}


//making the scoreboard
function drawScore() {
    ctx.font = "18px Arial"
    ctx.fillStyle = "#ffffff"
    ctx.fillText("Your score is: " +score,8,20)
}

function drawLives(){
    ctx.font =  "18px Arial"
    ctx.fillStyle = "#ffffff"
    ctx.fillText("Lives: "+lives,canvas.width-65,20)
}




//algo for the game
function draw() {
    ctx.clearRect(0,0,canvas.width, canvas.height)

    drawBricks()
    drawBall()
    drawPaddle()
    drawScore()
    collisionDetection()
    
    
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx
        
    }
    if (y + dy < ballRadius) {
        dy = -dy   
    }
    
    else if (y + dy > canvas.height - ballRadius) {
        if (x > paddleX && x < paddleX + paddleWidth) {
            if (y = y - paddleHeight) {
                dy = -dy
            }
        }
        else {
                alert("Press Enter to Restart Again")
                document.location.reload()
            }
    }
    if (rightPressed && paddleX < (canvas.width - paddleWidth)) { 
            paddleX += 7
    }
    else if (leftPressed && paddleX > 0) { 
        paddleX -= 7
    } 
    x += dx
    y += dy
}

setInterval(draw, 10)